import axios from "axios";

const API_URL = "http://localhost:8000"; // Replace with your backend API URL

const AuthService = {
  loginWithGoogle: () => {
    window.location.href = `${API_URL}/account/login`;
  },
};

export default AuthService;
