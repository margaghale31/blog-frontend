import React from "react";
import { Container, Grid, Typography, TextField, Button } from "@mui/material";

function About() {
  return (
    <div>
      <section style={{ padding: "50px 0" }}>
        <Container maxWidth="lg">
          <Typography variant="h2" align="center" gutterBottom>
            About The Company
          </Typography>
          <Grid container spacing={3}>
            <Grid item xs={12} lg={6}>
              <Typography variant="body1" paragraph>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error
                accusantium, deleniti repellendus ullam accusamus molestiae
                explicabo quo consequuntur assumenda, voluptatum!
              </Typography>
            </Grid>
            <Grid item xs={12} lg={6}>
              <Typography variant="body1" paragraph>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit
                nisi, tenetur numquam explicabo consectetur provident illo.
              </Typography>
            </Grid>
          </Grid>
          <Grid container justifyContent="center">
            <Grid item xs={12} md={6}>
              <img
                src="images/1x/asset-1.png"
                alt="Company"
                style={{ width: "100%", marginBottom: "30px" }}
              />
            </Grid>
          </Grid>
        </Container>
      </section>

      <section style={{ padding: "50px 0", backgroundColor: "#f5f5f5" }}>
        <Container maxWidth="lg">
          <Typography variant="h2" align="center" gutterBottom>
            The Podcaster
          </Typography>
          <Grid container spacing={3} justifyContent="center">
            <Grid item xs={12} md={6} lg={4}>
              <img
                src="images/person_1.jpg"
                alt="Jean Smith"
                style={{
                  width: "50%",
                  marginBottom: "20px",
                  borderRadius: "50%",
                  display: "block",
                  margin: "auto",
                }}
              />
              <Typography variant="h4" align="center" gutterBottom>
                Jean Smith
              </Typography>
              <Typography variant="body1" paragraph>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Pariatur ab quas facilis obcaecati non ea, est odit repellat
                distinctio incidunt, quia aliquam eveniet quod deleniti impedit
                sapiente atque tenetur porro?
              </Typography>
              <div style={{ textAlign: "center" }}>
                <a href="#" className="mr-3 text-dark">
                  <i className="bi bi-twitter" />
                </a>
                <a href="#" className="mr-3 text-dark">
                  <i className="bi bi-instagram" />
                </a>
                <a href="#" className="text-dark">
                  <i className="bi bi-facebook" />
                </a>
              </div>
            </Grid>
            {/* Repeat similar Grid items for other podcaster profiles */}
          </Grid>
        </Container>
      </section>

      <section
        style={{
          padding: "50px 0",
          backgroundImage: "url(images/hero_bg_1.jpg)",
          backgroundSize: "cover",
          backgroundPosition: "center",
        }}
      >
        <Container maxWidth="lg">
          <Grid container justifyContent="center" alignItems="center">
            <Grid item xs={12} md={6}>
              <Typography
                variant="h2"
                align="center"
                gutterBottom
                style={{ color: "#fff" }}
              >
                Subscribe
              </Typography>
              <Typography
                variant="body1"
                paragraph
                align="center"
                style={{ color: "#fff", marginBottom: "30px" }}
              >
                Lorem ipsum dolor sit amet, consectetur adipisicing elit nihil
                saepe libero sit odio obcaecati veniam.
              </Typography>
              <form>
                <TextField
                  id="subscribe-email"
                  label="Enter Email"
                  variant="outlined"
                  margin="normal"
                  fullWidth
                  InputProps={{
                    style: { backgroundColor: "transparent", color: "#fff" },
                  }}
                />
                <Button variant="contained" color="primary" type="submit">
                  Send
                </Button>
              </form>
            </Grid>
          </Grid>
        </Container>
      </section>
    </div>
  );
}

export default About;
