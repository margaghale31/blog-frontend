import React from "react";
import { Container, Grid, TextField, Button } from "@mui/material";
import "../css/Contact.scss";

function Contact() {
  return (
    <div>
      <section className="contact-section py-5">
        <Container data-aos="fade-up">
          <Grid container spacing={3}>
            <Grid item xs={12} md={7} className="mb-5">
              <form className="bg-white p-4">
                <Grid container spacing={2}>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      id="formFirstName"
                      label="First Name"
                      margin="normal"
                      fullWidth
                      required
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      id="formLastName"
                      label="Last Name"
                      margin="normal"
                      fullWidth
                      required
                    />
                  </Grid>
                </Grid>
                <TextField
                  id="formEmail"
                  label="Email"
                  type="email"
                  margin="normal"
                  fullWidth
                  required
                />
                <TextField id="formSubject" label="Subject" fullWidth />
                <TextField
                  id="formMessage"
                  label="Message"
                  margin="normal"
                  multiline
                  rows={5}
                  fullWidth
                />
                <Button
                  variant="contained"
                  color="primary"
                  type="submit"
                  size="large"
                  className="mt-3"
                >
                  Send Message
                </Button>
              </form>
            </Grid>
            <Grid item xs={12} md={5}>
              <div className="contact-info bg-white">
                <p className="mb-1">Address</p>
                <p className="mb-6">
                  203 Fake St. Mountain View, San Francisco, California, USA
                </p>
                <p className="mb-0">Phone</p>
                <p className="mb-4">
                  <a href="#">+1 232 3235 324</a>
                </p>
                <p className="mb-0">Email Address</p>
                <p className="mb-4">
                  <a href="#">youremail@domain.com</a>
                </p>
              </div>
            </Grid>
          </Grid>
        </Container>
      </section>
    </div>
  );
}

export default Contact;
