import React, { useState } from "react";
import { Container, Box, Grid, Button } from "@mui/material";
import AuthServices from "../../AuthServices/AuthServices";

const LoginRegistrationPage = () => {
  const [loginData, setLoginData] = useState({
    email: "",
    password: "",
  });

  const handleLoginChange = (e) => {
    const { name, value } = e.target;
    setLoginData({ ...loginData, [name]: value });
  };

  const handleGoogleLogin = async () => {
    try {
      await AuthServices.loginWithGoogle();
    } catch (error) {
      console.error("Google login error:", error.message);
      alert("Google login failed. Please try again.");
    }
  };

  return (
    <Container>
      <Box sx={{ py: 8 }}>
        <Grid container spacing={4} justifyContent="center">
          {/* Login Form */}

          {/* Button for Google Login */}
          <Grid item xs={12} md={5}>
            <Button
              variant="contained"
              color="secondary"
              size="large"
              fullWidth
              sx={{ mt: 2 }}
              onClick={handleGoogleLogin}
            >
              Login with Google
            </Button>
          </Grid>
        </Grid>
      </Box>
    </Container>
  );
};

export default LoginRegistrationPage;
