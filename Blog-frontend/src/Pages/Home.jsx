import React from "react";
import FeaturedGuests from "../Components/FeaturedGuests";
import PopularBlogs from "../Components/PopularBlogs";
import Episodes from "../Components/Episodes";
import { Info } from "@mui/icons-material";
import { Grid } from "@mui/material";
import "../css/Home.scss";

function Home({ nightMode }) {
  return (
    <div className="home">
      <Info />
      <>
        <Grid container spacing={3}>
          <Grid item xs={12} md={4}>
            <PopularBlogs nightMode={nightMode} />
          </Grid>
          <Grid item xs={12} md={6}>
            <Episodes nightMode={nightMode} />
          </Grid>
        </Grid>
      </>
      <FeaturedGuests nightMode={nightMode} />
    </div>
  );
}

export default Home;
