import React from "react";
import { Container, Grid, Typography, Divider } from "@mui/material";
import RelatedPost from "../Components/RelatedPost";
import PopularBlogs from "../Components/PopularBlogs";

function SinglePost() {
  return (
    <>
      <div>
        <div className="site-blocks-cover inner-page-cover bg-light mb-5">
          <Container>
            <Grid container justifyContent="center" alignItems="center">
              <Grid item xs={12} className="text-center">
                <Typography variant="subtitle2" component="span">
                  <a href="#">Mike Smith </a>
                  <span className="mx-2">•</span> Jun 25, 2020{" "}
                  <span className="mx-2">•</span> 1:30:20
                </Typography>
                <Typography variant="h1" component="h1" className="mb-3">
                  Tell Your Story to the World
                </Typography>
              </Grid>
            </Grid>
          </Container>
        </div>
        <Container className="play-wrap">
          <Grid container justifyContent="center" className="mb-5">
            <Grid item xs={12} md={8}>
              <Typography variant="h2" component="h2">
                This is your Blog Title
              </Typography>
            </Grid>
          </Grid>
        </Container>
        <Container>
          <Grid container justifyContent="space-between">
            <Grid item xs={12} md={4} lg={3} className="mb-5 mb-md-0">
              <div className="featured-user">
                <Typography variant="h3" gutterBottom>
                  Popular Blogs
                </Typography>
                <Divider />
                <ul className="list-unstyled">
                  {/* List of popular bloggers */}
                </ul>
              </div>
            </Grid>
            <Grid item xs={12} md={8} lg={7}>
              <Typography variant="body1" gutterBottom>
                <strong>Matt:</strong> Lorem ipsum dolor sit amet, consectetur
                adipisicing elit. Unde labore fugit earum beatae, qui autem
                asperiores harum quibusdam nulla in suscipit iure consequatur,
                laborum, inventore libero odit rem, dolore. Itaque?
              </Typography>
              {/* More dialogues */}
            </Grid>
          </Grid>
        </Container>
      </div>
      <RelatedPost />
    </>
  );
}

export default SinglePost;
