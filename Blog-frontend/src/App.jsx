import React, { useState } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Home from "./Pages/Home";
import Header from "./Components/Header";
import Footer from "./Components/Footer";
import LoginRegistration from "./Pages/LoginRegistration";
import About from "./Pages/About";
import Contact from "./Pages/Contact";
import SinglePost from "./Pages/SinglePost";
import "./App.css";

function App() {
  const [nightMode, setNightMode] = useState(false); // State for night mode

  const handleNightModeToggle = () => {
    setNightMode(!nightMode);
  };

  return (
    <div className={nightMode ? "night-mode" : ""}>
      <Router>
        <Header
          nightMode={nightMode}
          onNightModeToggle={handleNightModeToggle}
        />
        <Routes>
          {/* Pass nightMode to each page component */}
          <Route path="/" element={<Home nightMode={nightMode} />} />
          <Route path="/About" element={<About nightMode={nightMode} />} />
          <Route path="/Contact" element={<Contact nightMode={nightMode} />} />
          <Route
            path="/LoginRegistration"
            element={<LoginRegistration nightMode={nightMode} />}
          />
          <Route
            path="/SinglePost"
            element={<SinglePost nightMode={nightMode} />}
          />
        </Routes>
        <Footer />
      </Router>
    </div>
  );
}

export default App;
