import React from "react";

function RelatedPost() {
  return (
    <div className="site-section bg-light">
      <div className="container">
        <div className="row mb-5">
          <div className="col-md-12 text-center">
            <h2 className="font-weight-bold text-black">Related Blogs</h2>
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <div
              className="d-block podcast-entry bg-white mb-5"
              data-aos="fade-up"
            >
              <div
                className="image w-100"
                style={{
                  height: 300,
                  backgroundImage: 'url("images/img_2.jpg")',
                }}
              />
              <div className="text w-100">
                <h3 className="font-weight-light">
                  <a href="single-post.html">
                    Episode 07: How To Create Web Page Using Bootstrap 4
                  </a>
                </h3>
                <div className="text-white mb-3">
                  <span className="text-black-opacity-05">
                    <small>
                      By Mike Smith <span className="sep">/</span> 16 September
                      2017 <span className="sep">/</span> 1:30:20
                    </small>
                  </span>
                </div>
                <p className="mb-4">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                  Corrupti repellat mollitia consequatur, optio nesciunt
                  placeat. Iste voluptates excepturi tenetur, nesciunt.
                </p>
              </div>
            </div>
          </div>
          <div className="col-md-6">
            <div
              className="d-block podcast-entry bg-white mb-5"
              data-aos="fade-up"
            >
              <div
                className="image w-100"
                style={{
                  height: 300,
                  backgroundImage: 'url("images/img_3.jpg")',
                }}
              />
              <div className="text w-100">
                <h3 className="font-weight-light">
                  <a href="single-post.html">
                    Episode 07: How To Create Web Page Using Bootstrap 4
                  </a>
                </h3>
                <div className="text-white mb-3">
                  <span className="text-black-opacity-05">
                    <small>
                      By Mike Smith <span className="sep">/</span> 16 September
                      2017 <span className="sep">/</span> 1:30:20
                    </small>
                  </span>
                </div>
                <p className="mb-4">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                  Corrupti repellat mollitia consequatur, optio nesciunt
                  placeat. Iste voluptates excepturi tenetur, nesciunt.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default RelatedPost;
