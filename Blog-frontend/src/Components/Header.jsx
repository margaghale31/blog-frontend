import React from "react";
import { Link } from "react-router-dom";
import {
  AppBar,
  Toolbar,
  Typography,
  Button,
  IconButton,
  Menu,
  MenuItem,
  Container,
  useMediaQuery,
} from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import Brightness4Icon from "@mui/icons-material/Brightness4";
import Brightness7Icon from "@mui/icons-material/Brightness7";
import "../css/Header.scss";

function Header({ nightMode, onNightModeToggle }) {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const isMobile = useMediaQuery("(max-width:600px)");

  const handleMenuClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  return (
    <AppBar
      position="static"
      className={nightMode ? "header night-mode" : "header"}
    >
      <Container>
        <Toolbar className="toolbar">
          <Typography variant="h6" component={Link} to="/" className="brand">
            X-Blogs<span>.</span>
          </Typography>
          {isMobile ? (
            <IconButton
              edge="end"
              className="menu-icon"
              onClick={handleMenuClick}
            >
              <MenuIcon />
            </IconButton>
          ) : (
            <div className="nav-links">
              <Button component={Link} to="/" className="nav-button">
                Home
              </Button>
              <Button component={Link} to="/About" className="nav-button">
                About
              </Button>
              <Button component={Link} to="/Contact" className="nav-button">
                Contact
              </Button>
              <Button
                component={Link}
                to="/LoginRegistration"
                className="nav-button"
              >
                Login / Register
              </Button>
            </div>
          )}
          <div className="night-mode-toggle">
            <IconButton onClick={onNightModeToggle}>
              {nightMode ? (
                <Brightness7Icon
                  style={{ color: nightMode ? "white" : "black" }}
                />
              ) : (
                <Brightness4Icon />
              )}
            </IconButton>
          </div>
          <Menu
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleMenuClose}
            className="menu"
          >
            <MenuItem component={Link} to="/" onClick={handleMenuClose}>
              Home
            </MenuItem>
            <MenuItem component={Link} to="/About" onClick={handleMenuClose}>
              About
            </MenuItem>
            <MenuItem component={Link} to="/Contact" onClick={handleMenuClose}>
              Contact
            </MenuItem>
            <MenuItem
              component={Link}
              to="/LoginRegistration"
              onClick={handleMenuClose}
            >
              Login / Register
            </MenuItem>
          </Menu>
        </Toolbar>
      </Container>
    </AppBar>
  );
}

export default Header;
