import React from "react";
import {
  Container,
  Grid,
  Typography,
  Link,
  TextField,
  Button,
} from "@mui/material";

function Footer() {
  return (
    <footer
      style={{ backgroundColor: "#333", color: "#fff", padding: "50px 0" }}
    >
      <Container maxWidth="lg">
        <Grid container spacing={3}>
          <Grid item lg={4}>
            <Typography variant="h5" gutterBottom>
              About Us
            </Typography>
            <Typography variant="body1">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit.
              Praesentium animi, odio beatae aspernatur natus recusandae quasi
              magni eum voluptatem nam!
            </Typography>
          </Grid>
          <Grid item lg={3} sx={{ margin: "auto" }}>
            <Typography variant="h5" gutterBottom>
              Navigation
            </Typography>
            <ul style={{ listStyleType: "none", padding: 0 }}>
              <li>
                <Link href="#" color="inherit">
                  Podcasts
                </Link>
              </li>
              <li>
                <Link href="#" color="inherit">
                  Services
                </Link>
              </li>
              <li>
                <Link href="#" color="inherit">
                  About Us
                </Link>
              </li>
              <li>
                <Link href="#" color="inherit">
                  Blog
                </Link>
              </li>
              <li>
                <Link href="#" color="inherit">
                  Contact
                </Link>
              </li>
            </ul>
          </Grid>
          <Grid item lg={4}>
            <Typography variant="h5" gutterBottom>
              Subscribe
            </Typography>
            <Typography variant="body1">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod,
              quibusdam!
            </Typography>
            <form>
              <TextField
                id="subscribe-email"
                label="Enter Email"
                variant="outlined"
                margin="normal"
                fullWidth
              />
              <Button variant="contained" color="primary" type="submit">
                Subscribe
              </Button>
            </form>
          </Grid>
        </Grid>
        <Grid container justifyContent="center" sx={{ marginTop: "50px" }}>
          <Grid item>
            <Typography variant="body2" align="center">
              &copy; All rights reserved | This template is made with{" "}
              <i className="bi bi-heart-fill text-danger" /> by{" "}
              <Link
                href="https://colorlib.com"
                target="_blank"
                rel="noopener noreferrer"
                color="inherit"
              >
                Colorlib
              </Link>
            </Typography>
          </Grid>
        </Grid>
      </Container>
    </footer>
  );
}

export default Footer;
