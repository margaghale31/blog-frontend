import React from "react";
import { Container, Row, Col, Image } from "react-bootstrap";

function Info() {
  return (
    <div className="site-wrap">
      <Container className="pt-5 hero">
        <Row className="align-items-center text-center text-md-left">
          <Col lg={4} className="mb-4 mb-lg-0">
            <h1 className="mb-3 display-3">Tell Your Story to the World</h1>
            <p>
              Join with us! Login or Register. Lorem ipsum dolor sit amet,
              consectetur adipisicing elit. Delectus, ex!
            </p>
          </Col>
          <Col lg={8}>
            <Image src="images/1x/asset-1.png" fluid />
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default Info;
