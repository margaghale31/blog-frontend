import React from "react";
import {
  Grid,
  Card,
  CardContent,
  Typography,
  Link,
  Pagination,
  Box,
} from "@mui/material";

function Episodes() {
  return (
    <Grid container spacing={2} justifyContent="center">
      <Grid item xs={12}>
        <Grid container spacing={2} direction="column">
          {[1, 2, 3, 4, 5].map((episode, index) => (
            <Grid item xs={12} key={index}>
              <Card
                sx={{
                  boxShadow: "0 2px 4px rgba(0, 0, 0, 0.1)",
                  transition: "box-shadow 0.3s",
                  "&:hover": {
                    boxShadow: "0 4px 8px rgba(0, 0, 0, 0.2)",
                  },
                  width: "100%", // Ensure the card takes the full width
                }}
              >
                <CardContent>
                  <Typography variant="h6" component="h2">
                    <Link
                      href="/SinglePost"
                      sx={{ textDecoration: "none" }}
                      underline="hover"
                    >
                      Episode 0{episode}: How To Create Web Page Using Bootstrap
                      4
                    </Link>
                  </Typography>
                  <Typography variant="body2" color="textSecondary">
                    By Mike Smith / 16 September 2017 / 1:30:20
                  </Typography>
                </CardContent>
              </Card>
            </Grid>
          ))}
        </Grid>
      </Grid>
      <Box mt={4} display="flex" justifyContent="center" width="100%">
        <Pagination count={5} shape="rounded" />
      </Box>
    </Grid>
  );
}

export default Episodes;
