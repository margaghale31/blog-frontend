import React from "react";
import {
  Container,
  Typography,
  Box,
  Grid,
  Avatar,
  Card,
  CardContent,
  CardMedia,
  TextField,
  Button,
  styled,
} from "@mui/material";

const RootContainer = styled("div")(({ theme }) => ({
  padding: theme.spacing(4),
  backgroundColor: theme.palette.grey[200],
}));

const TitleTypography = styled(Typography)(({ theme }) => ({
  marginBottom: theme.spacing(4),
}));

const AvatarStyle = styled(Avatar)(({ theme }) => ({
  width: theme.spacing(12),
  height: theme.spacing(12),
  margin: "0 auto",
}));

const GuestCard = styled(Card)(({ theme }) => ({
  padding: theme.spacing(3),
}));

const SubscribeSection = styled(Box)(({ theme }) => ({
  backgroundImage: `url(images/hero_bg_1.jpg)`,
  backgroundSize: "cover",
  backgroundPosition: "center",
  padding: theme.spacing(8, 0),
  color: theme.palette.common.white,
}));

const SubscribeForm = styled("form")(({ theme }) => ({
  display: "flex",
  alignItems: "center",
  marginTop: theme.spacing(4),
}));

const SubscribeInput = styled(TextField)(({ theme }) => ({
  marginRight: theme.spacing(2),
  backgroundColor: "transparent",
  color: theme.palette.common.white,
  borderColor: theme.palette.common.white,
}));

function FeaturedGuests() {
  return (
    <RootContainer>
      <Container>
        <TitleTypography variant="h4" align="center">
          Featured Guests
        </TitleTypography>
        <Grid container spacing={4} justifyContent="center">
          <Grid item>
            <GuestCard>
              <CardMedia>
                <AvatarStyle alt="Guest Avatar" src="images/person_1.jpg" />
              </CardMedia>
              <CardContent>
                <Typography variant="h5" component="h3">
                  Megan Smith
                </Typography>
                <Typography variant="body1" color="textSecondary">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et,
                  iusto. Aliquam illo, cum sed ea? Ducimus quos, ea?
                </Typography>
              </CardContent>
            </GuestCard>
          </Grid>
          {/* Add more guest cards here */}
        </Grid>
      </Container>

      <SubscribeSection>
        <Container>
          <Grid container justifyContent="center" alignItems="center">
            <Grid item xs={12} md={6}>
              <Typography variant="h4" align="center">
                Subscribe
              </Typography>
              <Typography variant="body1" align="center" gutterBottom>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit nihil
                saepe libero sit odio obcaecati veniam.
              </Typography>
              <SubscribeForm>
                <SubscribeInput variant="outlined" placeholder="Enter Email" />
                <Button variant="contained" color="primary">
                  Send
                </Button>
              </SubscribeForm>
            </Grid>
          </Grid>
        </Container>
      </SubscribeSection>
    </RootContainer>
  );
}

export default FeaturedGuests;
