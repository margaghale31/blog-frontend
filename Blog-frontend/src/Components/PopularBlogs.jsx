import React from "react";
import {
  Container,
  Grid,
  Box,
  List,
  ListItem,
  ListItemAvatar,
  Avatar,
  ListItemText,
  Typography,
} from "@mui/material";

const PopularBlogs = () => {
  return (
    <Box className="site-section" py={4}>
      <Container>
        <Grid container spacing={4}>
          <Grid item xs={12} md={8}>
            <Box mb={5}>
              <Typography variant="h4" component="h3" gutterBottom>
                Popular Blogs
              </Typography>
              <List>
                <ListItem button component="a" href="#">
                  <ListItemAvatar>
                    <Avatar src="images/person_1.jpg" alt="Claire Stanford" />
                  </ListItemAvatar>
                  <ListItemText
                    primary="Claire Stanford"
                    secondary="32,420 podcasts"
                  />
                </ListItem>
                <ListItem button component="a" href="#">
                  <ListItemAvatar>
                    <Avatar src="images/person_2.jpg" alt="Dianne Winston" />
                  </ListItemAvatar>
                  <ListItemText
                    primary="Dianne Winston"
                    secondary="12,381 podcasts"
                  />
                </ListItem>
                {/* Add more ListItem components for other podcasters */}
              </List>
            </Box>
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
};

export default PopularBlogs;
