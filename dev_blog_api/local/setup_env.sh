#!/bin/bash

# Define file paths
template_env="TEMPLATE.env.txt"

# Use the first argument as the stage name if provided, else default to "local"
arg_stage="${1:-local}"

stage=$(echo "$arg_stage" | tr '[:lower:]' '[:upper:]')

local_env_file="$stage.env.txt"
output_env=".env"

echo "Recognized stage: $stage"
echo "Will load values from $local_env_file file and then try \$${stage}_KEY_NAME"

# Clear or create the output file
> "$output_env"

# Read each line from TEMPLATE.env.txt and ensures that the loop also processes the last line, even if it lacks a newline character.
while IFS='=' read -r template_key template_value || [ -n "$template_key" ]
do
    echo $template_key
   # Initialize a flag to check if a replacement was made
    replaced=0

    # Check each line in local.env for a matching key 
    while IFS='=' read -r local_key local_value || [ -n "$local_key" ]
    do
        if [ "$template_key" = "$local_key" ]; then
            # If a match is found, write the local.env value to the output file
            echo "[Info] Matched key from $local_env_file file for key $local_key"
            echo "$local_key=$local_value" >> "$output_env"
            replaced=1
            break
        fi
    done < "$local_env_file"

    # If no replacement was made in local.env, check for an environment variable
    if [ $replaced -eq 0 ]; then
        # build stage level variable
        # if key is SOME_KEY and stage is ABC then it will replace value by value present in ABC_SOME_KEY
        stage_var=${stage}_$template_key
        eval value_in_var=\$$stage_var
        env_value=$(echo $value_in_var)
        if [ -n "$env_value" ]; then
            echo "[Info] Matched key from shell for key $stage_var in replacement of $template_key"
            # If the environment variable exists, use its value
            echo "$template_key='$env_value'" >> "$output_env"
        else

            warning_first_part="[Warning] No value found for key $template_key in $local_env_file"
            warning_second_part="and also in environment (for variable \$$stage_var)"
            warning_third_part="Will put \$$stage_var as value instead."
            echo "$warning_first_part $warning_second_part. $warning_third_part"
            # If the environment variable does not exist, use the original TEMPLATE.env line
            echo "$template_key=\$$stage_var" >> "$output_env"
        fi
    fi
done < "$template_env"
mv $output_env ../$output_env