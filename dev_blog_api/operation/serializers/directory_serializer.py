from rest_framework import serializers


class DirectoryListSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    space_id = serializers.IntegerField()
    name=serializers.CharField()
    created_at=serializers.DateTimeField()
    updated_at=serializers.DateTimeField()

class DirectoryCreateSerializer(serializers.Serializer):
    name=serializers.CharField()
    space_id = serializers.IntegerField()

class DorectoryDetailSerializer(serializers.Serializer):
    id=serializers.IntegerField()
    space_id=serializers.IntegerField()
    name=serializers.CharField()
    created_at=serializers.DateTimeField()
    updated_at=serializers.DateTimeField()



