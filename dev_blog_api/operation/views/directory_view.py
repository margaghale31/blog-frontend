from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from operation.services.directory_service import DirectoryService
from operation.serializers.directory_serializer import (
    DirectoryListSerializer, 
    DirectoryCreateSerializer,
    DorectoryDetailSerializer )

from response.error_response import ErrorResponse

class DirectoryListView(APIView):
    def get(self, request, space_id):
        space=DirectoryService.list(space_id)
        serializer=DirectoryListSerializer(space, many=True)
        return Response(data={'data':serializer.data}, status=status.HTTP_200_OK)
    
    def post(self, request, space_id):
        space, error = DirectoryService.get_space(space_id)
        if not space:
            return ErrorResponse(errors=error, status_code=404)

        request.data['space_id']=space_id
        serializer = DirectoryCreateSerializer(data=request.data)

        if serializer.is_valid():
            data, error=DirectoryService.create(data=serializer.validated_data)
            if data:
                serializer=DorectoryDetailSerializer(data)
                return Response(data={'data':serializer.data}, status=status.HTTP_200_OK)
            
            return ErrorResponse(errors=error, status_code=400)
            
        return Response(data={'error':serializer.errors}, status=status.HTTP_400_BAD_REQUEST)