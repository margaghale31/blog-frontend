from operation.models.directory import Directory
from operation.models.space import Space
from response.error_response import Error

error = Error()
class DirectoryService:
    def list(space_id):
        return Directory.objects.filter(space=space_id)
    
    def get_space(space_id):
        space = Space.objects.filter(id=space_id).first
        if space:
            return space, None
        return error.add_errors(name='Space Error', message='Not Found')
    
    # Create the directory
    def create(data):
        try:
            directory = Directory.objects.create(**data)
            return directory, None
        except Exception as e:
            return error.add_errors(name='Directory creation failed', message={str(e)}), None


