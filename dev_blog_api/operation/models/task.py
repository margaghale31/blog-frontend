from django.db import models
from operation.models.directory import List

TASK_STATUS_CHOICE = [
    ('todo', 'TODO'),
    ('inprogress', 'INPROGRESS'),
    ('ready to review', 'READY TO REVIEW'),
    ('merged', 'MERGED'),
    ('deploy', 'DEPLOY'),
    ('completed', 'COMPLETED')
]

class Task(models.Model):
    name = models.CharField(max_length=100, blank=False, null=False, unique=True)
    description = models.CharField(max_length=500, blank=False, null=False, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    due_date= models.DateTimeField(default=None)
    is_active= models.BooleanField(default=True)
    status= models.CharField(max_length=30, choices=TASK_STATUS_CHOICE)
    todo = models.ForeignKey(List, on_delete=models.DO_NOTHING)

class SubTask(models.Model):
    name = models.CharField(max_length=100, blank=False, null=False, unique=True)
    description = models.CharField(max_length=500, blank=False, null=False, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    is_active= models.BooleanField(default=True)
    updated_at = models.DateTimeField(auto_now=True)
    due_date= models.DateTimeField(default=None)
    status= models.CharField(max_length=30, choices=TASK_STATUS_CHOICE)
    task = models.ForeignKey(Task, on_delete=models.DO_NOTHING)