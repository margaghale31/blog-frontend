from django.db import models
from operation.models.space import Space
from django.core.exceptions import ValidationError


class Directory(models.Model):
    name = models.CharField(max_length=100, blank=False, null=False, unique=True)
    space = models.ForeignKey(Space, on_delete=models.DO_NOTHING)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True) 


class List(models.Model):
    name = models.CharField(max_length=100, blank=False, null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True) 
    space = models.ForeignKey(Space, related_name='lists', on_delete=models.DO_NOTHING, blank=True, null=True)
    directory = models.ForeignKey(Directory, related_name='lists', on_delete=models.DO_NOTHING, blank=True, null=True)

    def clean(self):
        if self.space is None and self.directory is None:
            raise ValidationError("A list must be associated with either a directory or a space.")